#!/usr/bin/env groovy
def scmVars

node {
  stage('Checkout') {
    retry(3) { scmVars = checkout scm }
    packageFile = readJSON file: 'package.json'
    echo "Building branch ${scmVars.GIT_BRANCH}"
    echo "Building version ${packageFile.version}"
  }

  stage('Build') {
    withEnv(["PATH+jdk=${tool 'NodeJS'}/bin"]) {
      sh "npm install"
      sh "npm run build-ci"
      archiveArtifacts artifacts: 'dist/**/*'
    }
  }

  stage('Tests') {
    withEnv(["PATH+jdk=${tool 'NodeJS'}/bin"]) {
      if (env.TEST_WITH_COVERAGE) {
        sh "npm run test-ci"
        archiveArtifacts artifacts: 'coverage/lcov-report/*'
      } else {
        sh "npm run test-ci-without-coverage"
      }
    }
  }

  stage("SonarQube analysis") {
    withEnv(["PATH+jdk=${tool 'NodeJS'}/bin"]) {
      withSonarQubeEnv('SonarCloud') {
        sh "npm run sonar-scanner"
      }
    }
  }

}
